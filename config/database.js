module.exports = {
    enabled  : String(process.env.DATABASE_ENABLED).toLowerCase().trim() === "true",
    type     : String(process.env.DATABASE_TYPE),
    host     : String(process.env.DATABASE_HOST),
    port     : parseInt(process.env.DATABASE_PORT),
    database : String(process.env.DATABASE_NAME),
    schema   : String(process.env.DATABASE_SCHEMA),
    username : String(process.env.DATABASE_USERNAME),
    password : String(process.env.DATABASE_PASSWORD),
    logging  : String(process.env.DATABASE_LOGGING).toLowerCase().trim() === 'true'
}
