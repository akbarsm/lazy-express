module.exports = {
	source: process.env.JWT_SOURCE,
	hash: process.env.JWT_HASH,
	expiration: process.env.JWT_EXPIRATION
}
