module.exports = {
    cors: String(process.env.SERVER_CORS).toLowerCase().trim() === "true",
    port: String(process.env.SERVER_PORT),
    url: String(process.env.SERVER_URL)
}
