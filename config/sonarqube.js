module.exports = {
    sonarscanner: process.env.SONARQUBE_SONARSCANNER,
    sonarserver: process.env.SONARQUBE_SONARSERVER,
    sonarport: process.env.SONARQUBE_SONARPORT
}
