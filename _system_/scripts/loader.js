require('dotenv')

let fs = require("fs")

fs.readdirSync("./_system_/modules").forEach((each, index) => {

	let name = String(each)
	if (name.endsWith(".js") && fs.lstatSync(`./_system_/modules/${name}`).isFile()) {
		if (index < 7) {
			let modules = require(`../modules/${name}`)

			Object.keys(modules).forEach((value, key) => {
				if(Object.keys(modules[key]).includes('action')) {
					global[modules[key].name] = modules[key].action
				} else {
					modules[key].autoload()
				}
			})
		}

	}
})

global.args = process.argv.slice(2)

global.controller = (subdir = "") => {
	return $root("controller/" + subdir)
}

global.model = (subdir = "") => {
	return $root("model/" + subdir)
}

process.stdout.write("\x1Bc")
