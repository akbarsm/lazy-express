require("./loader")

const chalk = require('chalk')

let content = (identifier) => {
	return `sonar.projectKey=${$random(40, "abcdef1234567890")}
sonar.projectName=LazyExpress ${identifier}
sonar.sources=.
#sonar.projectVersion=0.0.1`
}

$file.write($root("sonar-project.properties"), content($date.format(new Date, "YY-MM-DD (HH:mm:ss)")))

console.log(chalk.yellowBright(`.-----------------------------------------------------------------------------.`))
console.log(chalk.yellowBright(`| WARNING : Make sure you have set the 'SONARQUBE_SONARSERVER' binary to .env |`))
console.log(chalk.yellowBright(`'-----------------------------------------------------------------------------'`))
console.log(chalk.yellowBright(``))
console.log('Please wait while sonar scanner is scanning your project...')
console.log('')

try {
	let {spawn} = require("child_process"),
		child   = spawn("sonar-scanner"),
		url     = ""

	child.stdout.setEncoding("utf-8").on("data", (chunk) => {
		chalk.blueBright(chunk)

		if (chunk.toString().startsWith("INFO: ANALYSIS SUCCESSFUL, you can browse ")) {
			url = chunk.toString().replace("INFO: ANALYSIS SUCCESSFUL, you can browse ", "")
		}
	})

	child.stdout.on("close", () => {
		console.log(chalk.blueBright(`.-----------------------.`))
		console.log(chalk.blueBright(`| Sonarqube scan result |`))
		console.log(chalk.blueBright(`'-----------------------'`))
		console.log(chalk.blueBright(`URL : ${url}`))

		require("open")(url)
	})

} catch (error) {
	chalk.blueBright(`ERROR : ${error.message}`)
}
