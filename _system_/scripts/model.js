require("./loader")
const chalk = require('chalk')

let content = (name, table, alias, id) => `class ${name} extends LazyDB {

	info() {
		return {
			table : '${table}',
			alias : '${alias}',
			primary: '${id}'
		}
	}
}

module.exports = ${name}`

if (args.length === 4) {
	let modelName = args[0]
	let tableName = args[1]
	let aliasName = args[2]
	let primaryColumn = args[3]

	let path = `${$root("model")}/${modelName}.js`

	$file.write(path, content(modelName, tableName, aliasName, primaryColumn))
	console.log(`Model '${modelName}' generated...`)

} else {
	$log.error('CLI.model', `model creation needs 4 parameters (ModelName, TableName, TableAlias, PrimaryColumn), example 'npm run make:model User users usr id'`)
}
