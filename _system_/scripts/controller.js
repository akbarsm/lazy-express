require("./loader")
const chalk = require('chalk')

let content = (controllerName) => `module.exports = {

	get: {
		action: async (req, res, next) => {

			let rules = {
			}

			await $validate(req, res, rules, async (data) => {
				try {
					
				} catch (error) {
					$log.error('/${controllerName}.get', error.message)
					res('/${controllerName}.get: ' + error.message)
				}
			})
		}
	},
	
}`

if (args.length === 1) {
	let controllerName = args[0]
	let path = `${$root("controller")}/${controllerName}.js`

	$file.write(path, content(controllerName))
	console.log(`Controller '${controllerName}' generated... (dont forget to set its routes in config/route)`)

} else {
	$log.error('CLI.controller', `controller creation needs only 1 parameters (ControllerName), example 'npm run make:controller UserController'`)
}
