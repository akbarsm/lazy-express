require("./loader");

if (args.length === 1) {
	if (["all", "controller"].includes(args[0])) {
		$file.delete($root('controller'));
		$file.mkdirs($root('controller'));

		console.log(`Controller cleared!`);

	}

	if (["all", "model"].includes(args[0])) {
		$file.delete($root('model'));
		$file.mkdirs($root('model'));

		console.log(`Model cleared!`);
	}
} else {
	$log.error(`CLI.clear`, `command needs only 1 parameter`);
}
