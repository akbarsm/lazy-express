module.exports = [
	{
		name: '$empty',
		action: (param) => {
			var isEmptyArray    = typeof param === 'object'     && param !== null && Array.isArray(param) && !param.length,
				isEmptyObject   = typeof param === 'object'     && param !== null && !Object.keys(param).length,
				isEmptyString   = typeof param === 'string'     && !param.length,
				isFalse         = typeof param === 'boolean'    && !param,
				isNull          = param === null,
				isZero          = typeof param === 'number'     && param === 0,
				isUndefined     = typeof param === 'undefined'
		
			return (isEmptyArray || isEmptyObject || isEmptyString || isFalse || isNull || isZero || isUndefined)
		}
	},
	{
		name: '$foreach',
		action: (object, callback) => {
			object.forEach(callback)
		}
	},
	{
		name: '$forEach',
		action: (object, callback) => {
			object.forEach(callback)
		}
	},
	{
		name: '$isAlpha',
		action: (param) => {
			return String(param).search("^[\\s+a-zA-Z]+$") >= 0
		}
	},
	{
		name: '$isAlphaDash',
		action: (param) => {
			return String(param).search("^[-_\\s+a-zA-Z]+$") >= 0
		}
	},
	{
		name: '$isAlphaNumeric',
		action: (param) => {
			return String(param).search("^[0-9a-zA-Z]+$") >= 0
		}
	},
	{
		name: '$isArray',
		action: (param) => {
			return typeof param === "object" && param instanceof Array
		}
	},
	{
		name: '$isEmail',
		action: (param) => {
			return String(param).search("^[a-zA-Z_\\.]+@[a-zA-Z]+(\\.[a-zA-Z]+)+$") >= 0
		}
	},
	{
		name: '$isFloat',
		action: (param) => {
			return !isNaN(parseFloat(param)) && String(param).search("^[0-9]+(\\.{1}[0-9]+){0,1}$") >= 0
		}
	},
	{
		name: '$isFunction',
		action: (param) => {
			return typeof param === "function"
		}
	},
	{
		name: '$isNumeric',
		action: (param) => {
			return typeof param === "number" || String(param).search("\\D+") === -1
		}
	},
	{
		name: '$isNull',
		action: (param) => {
			return param === null
		}
	},
	{
		name: '$isObject',
		action: (param) => {
			return typeof param === "object"
		}
	},
	{
		name: '$isString',
		action: (param) => {
			return typeof param === "string"
		}
	},
]
