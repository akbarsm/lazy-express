let validators = {}

let parseValues = (data, key, keyIsArray, valueIsArray) => {
	let values = []

	if (!keyIsArray) {
		values.push(data[key])
	} else {
		if (valueIsArray) {
			let [base, child] = key.split(".")
			data[base].forEach((each) => {
				values.push(each[child] || null)
			})
		} else {
			values.push(null)
		}
	}

	return values
}

$file.list($root("_system_/modules/12.lazy-validator"), (name, path) => {
	let validator = require(path)
	validators[validator.prefix] = validator.action
})

module.exports = [
	{
		name: '$validate',
		action: async (req, res, $rules, callback, onError = (errorMessages) => { res(errorMessages); res().end() }) => {
			let messages = []
			let sourceData = { ...req.body(), ...req.param(), ...req.query(), ...req.files() }
			let data = {}
			let aliases = {}
			let parsedRules = {}


			$foreach($rules, (value, key) => {
				let keyIndex = key.contains(':') ? 1 : 0
				let originalKey = key.split(":")[0]
				let parsedKey = key.split(":")[keyIndex]

				aliases[originalKey] = parsedKey
				data[originalKey] = sourceData[originalKey]
				parsedRules[originalKey] = value
			})

			for (let eachKey of $rules.$keys()) {
				let rules = $rules[eachKey]

				let key = eachKey.split(':')[0]
				if (rules.includes("required")) {
					rules = rules.filter((each) => {
						return each.trim() !== "required"
					})
					rules.unshift("required")
				}

				let requiredError = false
				for (let rawRule of rules) {
					if (!requiredError) {
						let rule = rawRule.split(":")[0]
						let options = rawRule.split(":")[1] || null

						if (validators.hasKey(rule)) {
							let keyIsArray = key.contains("\\.")
							let valueIsArray = typeof data[key] === "object" && data[key].length === 0
							let validationResult = await validators[rule](req, data, options, key, rules, parseValues(data, key, keyIsArray, valueIsArray), keyIsArray, valueIsArray)

							if (validationResult instanceof Object) {
								data[key] = validationResult
							}

							messages.push(typeof validationResult === "string" ? validationResult : null)
							if (rule === "required" && typeof validationResult === "string") {
								requiredError = true
							}

						} else {
							$log.warning('LazyValidator', `'${rule}' rule does not exists!\n`)
						}
					}
				}
			}

			messages = messages.filter((each) => {
				return each !== null
			})

			if (messages.length === 0) {

				let parsedData = {}
				aliases.forEach((aliasKey, originalKey) => {
					parsedData[aliasKey] = data[originalKey]
				})

				await callback(parsedData)
			} else {
				onError(messages)
			}


		}
	}
]
