const jwt = require('express-jwt')
const token = require('jsonwebtoken')

module.exports = [
	{
		name: '$jwt',
		action: {
			hash: $config('jwt').hash,
			expiration: $config('jwt').expiration,
			decode: (string) => {
				if ($jwt.hash === "" || $jwt.expiration === "") {
					$log.error('$jwt.decode', 'JWT configuration not set')
					return string;
				}
				return token.decode(string);
			},
			encode: (string) => {
				if ($jwt.hash === "" || $jwt.expiration === "") {
					$log.error('$jwt.decode', 'JWT configuration not set')
					return string;
				}

				return token.sign(string, $jwt.hash, { expiresIn: $jwt.expiration });
			},
			initJwt : async () => {
				if ($config('jwt').source === 'config') {
					$jwt.expiration = $config('jwt').expiration
					$jwt.hash = $config('jwt').hash
				} else {
					let dbConfig = $db('config')
					let expiration= await dbConfig.where('name', 'jwt_expiration').$first()
					let hash = await dbConfig.where('name', 'jwt_hash').$first()

					$jwt.expiration = expiration.value
					$jwt.hash = hash.value
				}
			},
			middleware: () => {
				return jwt({
					secret: $jwt.hash,
					expire: $jwt.expiration,
					algorithms: ["sha1", "RS256", "HS256"]
				})
			}
		}		
	}
]
