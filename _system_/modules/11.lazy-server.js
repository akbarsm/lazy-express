const chalk = require('chalk')
const formatter = require('fast-printf').printf

const express = require('express')
const app = express()

const cors = require('cors')({
	origins: ["*"],
	allowHeaders: ["Authorization"],
	exposeHeaders: ["Authorization"]
})
const multer = require('multer')()
const xmlParser = require('express-xml-bodyparser')

const fileType = require('detect-file-type')

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(xmlParser({
	explicitArray: false,
	normalize: false,
	normalizeTags: false,
	trim: true
}))
app.use(express.static('public'))

if ($config("server").cors) {
	app.use(cors);
}

module.exports = [
	{
		name: '$server',
		action: {
			registerRoutes: () => {
				let longestName = 0
				let longestType = 0
				$foreach($config('routes'), (urls, controller) => {
					$foreach(urls, (type, url) => {
						if (url.length > longestName) longestName = url.length
						if (type[0].length > longestType) longestType = type[0].length
					})
				})

				longestType += 4
				$foreach($config('routes'), (urls, controllerName) => {
					$foreach(urls, (type, url) => {

						if ($file.exists($root('controller/' + controllerName + '.js'))) {
							let color = {
								get: chalk.blueBright(`%-${longestType}s`),
								post: chalk.greenBright(`%-${longestType}s`),
								put: chalk.yellowBright(`%-${longestType}s`),
								delete: chalk.redBright(`%-${longestType}s`)
							}

							let controller = $controller(controllerName)
							let hasMiddleware = type.length === 3
							let middlewareList = type.length === 3 ? type[2].join(', ') : ''
							let middlewares = [multer.any()]
							let hasJwt = false

							if (hasMiddleware) {
								$foreach(type[2], (middlewareName) => {
									if (middlewareName.toLowerCase().trim() === 'jwt') {
										middlewares.push($jwt.middleware())
										hasJwt = true
									} else {
										middlewares.push(require($root('middleware/' + middlewareName)))
									}
								})
							}

							let nameFormat = chalk.greenBright(`%-${longestName}s`)
							console.log(formatter(`Registering endpoint ${nameFormat} ... ${color[type[0].toLowerCase()]} ${hasMiddleware ? '[%s]' : ''}`, url, type[0], middlewareList))

							app[type[0].toLowerCase().trim()](`/${url.trimLeft('/')}`, middlewares, async (req, res, next) => {
								try {
									let request = {
										query: (key = '', defaultValue = '') => {
											return key ? (req.query[key] || defaultValue) : req.query
										},

										param: (key = '', defaultValue = '') => {
											return key ? (req.params[key] || defaultValue) : req.params
										},

										body: (key = '', defaultValue = '') => {
											return key ? (req.body[key] || defaultValue) : req.body
										},

										header: (key = '', defaultValue = '') => {
											return key ? (req.headers[key] || defaultValue) : req.headers
										},

										user: () => {
											if (!hasJwt && req.headers.hasKey('authorization') && req.headers.authorization.startsWith('Bearer') && req.headers.authorization.split(' ').length > 0) {
												return $jwt.decode(req.headers.authorization.split(' ')[1]).except('iat', 'exp')
											} else {
												return req.user.except('iat', 'exp')
											}
										},

										files: (key = '') => {
											let file = {}

											if (req.hasKey('files')) {
												$foreach(req.files, (each) => {
													let info = {
														ext: each.originalname.split('.').pop(),
														mime: each.mimetype
													}

													fileType.fromBuffer(each.buffer, (error, result) => {
														if (result) info = result
													})

													file[each.fieldname] = {
														name: each.originalname,
														ext: info.ext,
														mime: info.mime,
														buffer: () => {
															return each.buffer;
														},
														content: () => {
															return each.buffer.toString();
														},
														sizeInKB: +(each.size / 1024).toFixed(2),
														sizeInMB: +(each.size / 1024 / 1024).toFixed(2)
													}
												})
											}

											return key ? file[key] : file
										}
									}

									let response = (message, status = 200) => {
										if (message) {
											if (!res._headerSent) {
												res.status(status).send(typeof message === "number" ? String(message) : message);
											}
										} else {
											return {
												attach: (filename = "", content = "") => {
													if (!res.headersSent) {
														res.status(200).attachment(filename).send(content);
													}
												},

												end: (paramMessage, paramStatus = 200) => {
													if (!res._headerSent) {
														res.status(paramStatus).end(paramMessage);
													}
												},

												redirect: (targetURL) => {
													res.redirect(targetURL);
												},
											};
										}
									}

									await controller[type[1]].action(request, response, next)
								} catch (error) {
									$log.error(`Controller ${controllerName}:`, error.message)
								} finally {
									res.end()
								}
							})

							/* JWT Error Handler */
							app.use((error, req, res, next) => {
								if (error.status === 401) {
									if (error.inner.name === "TokenExpiredError") {
										res.status(401).send({ status: "ERROR", message: "Token expired..." })
									} else if (error.inner.name === "JsonWebTokenError") {
										res.status(401).send({ status: "ERROR", message: "Invalid Token Algorithm" })
									} else {
										res.status(401).send({ status: "ERROR", message: "No authorization token was found..." })
									}
								}
							})
						} else {
							$log.warning('LazyServer.registerRoutes', `Controller '${controllerName}' not found`)
						}

					})
					console.log('')
				})
				console.log(``)
			},

			start: () => {
				app.listen($config('server').port, () => {
					console.log(`.-------------.`)
					console.log(`| ${chalk.bold(chalk.yellow('Server Info'))} |`)
					console.log(`'-------------'`)
					console.log(``)
					console.log(` Port\t\t: ${chalk.greenBright($config('server').port)}`)
					console.log(` CORS\t\t: ${$config('server').cors ? chalk.greenBright('ENABLED') : chalk.redBright('DISABLED')}`)
					console.log(` URL\t\t: ${chalk.greenBright($config('server').url)}`)
					console.log(``)
					console.log(``)

					console.log(`.-------------------------.`)
					console.log(`| ${chalk.bold(chalk.yellow('Your Actions Start Here'))} |`)
					console.log(`'-------------------------'`)
					console.log(``)

					require($root('actions'))
				})
			}
		},

	}
]
