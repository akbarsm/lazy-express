const chalk = require('chalk')
const formatter = require('fast-printf').printf

module.exports = [
	{
		name: '$log',
		action: {
			clear: () => {
				process.stdout.write("\x1Bc")
			},

			error: (module, message) => {
				let moduleName = chalk.black(` ${module} `)
				console.log(`${chalk.bgWhiteBright(moduleName)} ${chalk.redBright(message)}`)
			},

			format: (format, ...params) => {
				console.log(formatter(format, params))
			},

			info: (module, message) => {
				let moduleName = chalk.black(` ${module} `)
				console.log(`${chalk.bgWhiteBright(moduleName)} ${chalk.blueBright(message)}`)
			},

			success: (module, message) => {
				let moduleName = chalk.black(` ${module} `)
				console.log(`${chalk.bgWhiteBright(moduleName)} ${chalk.greenBright(message)}`)
			},

			warning: (module, message) => {
				let moduleName = chalk.black(` ${module} `)
				console.log(`${chalk.bgWhiteBright(moduleName)} ${chalk.yellowBright(message)}`)
			},
		}
	}
]
