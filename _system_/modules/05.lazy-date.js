const moment = require('moment-timezone')

module.exports = [
	{
		name: '$date',
		action: {
			format: (date, format) => {
				return moment(date).format(format)
			},

			moment: () => {
				return moment
			},

			now: (withTime = false) => {
				return moment().tz('Asia/Jakarta').format(`YYYY-MM-DD${withTime ? " HH:mm:ss" : ""}`)
			},

			parse: (string, format) => {
				return moment(string, format).tz('Asia/Jakarta').toDate()
			}
		}
	},
]
