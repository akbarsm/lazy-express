module.exports = (instance) => (...column) => {
	instance.setHaving({having : column})

	return instance
}
