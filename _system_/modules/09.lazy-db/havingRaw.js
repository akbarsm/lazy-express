module.exports = (instance) => (...column) => {
	instance.setHaving({havingRaw : column})

	return instance
}
