module.exports = (instance) => (...column) => {
	instance.setWhere({whereNotBetween : column})

	return instance
}
