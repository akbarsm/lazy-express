module.exports = (instance) => (...column) => {
	instance.setWhere({whereBetween : column})

	return instance
}
