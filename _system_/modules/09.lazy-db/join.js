module.exports = (instance) => (...column) => {
	instance.setJoin({innerJoin : column})

	return instance
}
