module.exports = (instance) => (...column) => {
	instance.setWhere({orWhereBetween : column})

	return instance
}
