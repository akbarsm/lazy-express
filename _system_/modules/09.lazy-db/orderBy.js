module.exports = (instance) => (column, direction) => {
	instance.setOrder(column, direction)

	return instance
}
