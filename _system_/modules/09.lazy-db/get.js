module.exports = (instance) => async () => {
	let result = undefined

	try {
		let alias = instance.info().alias
		if (alias.length > 0) {
			alias = `as ${alias}`
		}

		let knex = $knex(`${instance.info().table} ${alias}`.trim())

		if (instance.getSelect().length > 0) {
			knex.select(...instance.getSelect())
		}

		if (instance.getDistinct().length > 0) {
			knex.distinct(...instance.getDistinct())
		}

		for (let join of instance.getJoin()) {
			$foreach(join, (param, joinType) => {
				knex[joinType](...param)
			})
		}

		[instance.getWhere(), instance.getHaving()].forEach((eachCondition) => {
			for (let condition of eachCondition) {
				$foreach(condition, (param, conditionType) => {
					knex[conditionType](...param)
				})
			}
		})

		if (instance.getOffset() != -1) {
			knex.offset(instance.getOffset())
		}

		if (instance.getLimit() != -1) {
			knex.limit(instance.getLimit())
		}

		if (instance.getOrder().$keys().length > 0) {
			let column = instance.getOrder().$keys()[0]
			let direction = instance.getOrder().$values()[0]

			knex.orderBy(column, direction)
		}

		if (instance.getGroup().length > 0) {
			knex.groupBy(...instance.getGroup())
		}

		result = await knex

		instance.reset()
	} catch (error) {
		$log.error('LazyDB.get', error.message)
	}

	let results = []
	$foreach(result, (each, index) => {
		let temp = Object.create(instance)
		temp.assign(each)

		results.push(temp)
	})

	return results
}
