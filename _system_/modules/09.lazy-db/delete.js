module.exports = (instance) => async (data = {}) => {
	let result = undefined

	try {
		let alias = instance.info().alias		
		if(alias.length > 0){
			alias = `as ${alias}`
		}

		let knex = $knex(`${instance.info().table} ${alias}`.trim())
		let hasCondition = false

		for (let condition of instance.getWhere()) {
			$foreach(condition, (param, conditionType) => {
				knex[conditionType](...param)
				hasCondition = true
			})
		}

		for (let condition of instance.getHaving()) {
			$foreach(condition, (param, conditionType) => {
				knex[conditionType](...param)
				hasCondition = true
			})
		}

		if(!hasCondition){
			data.forEach((value, key) => {
				knex.where(key, value)
			})
		}

		result = await knex.delete()
		result = result > 0

		instance.reset()
	} catch (error) {
		$log.error('LazyDB.delete', error.message)
	}

	return result
}
