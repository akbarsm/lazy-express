module.exports = (instance) => (...column) => {
	instance.setWhere({whereNot : column})

	return instance
}
