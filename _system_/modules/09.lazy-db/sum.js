module.exports = (instance) => async (column) => {
	let result = undefined

	try {
		result = await $knex(instance.info().table).sum(column)
		result = +result[0].sum
		instance.reset()
	} catch (error) {
		$log.error('LazyDB.sum', e.message)
	}

	return result
}
