module.exports = (instance) => (...column) => {
	instance.setHaving({havingIn : column})

	return instance
}
