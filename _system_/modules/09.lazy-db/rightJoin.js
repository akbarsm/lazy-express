module.exports = (instance) => (...column) => {
	instance.setJoin({rightJoin : column})

	return instance
}
