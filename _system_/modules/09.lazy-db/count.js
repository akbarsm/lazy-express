module.exports = (instance) => async (column) => {
	let result = undefined

	try {
		result = await $knex(instance.info().table).count(column)
		result = +result[0].count
		instance.reset()
	} catch (error) {
		$log.error('LazyDB.count', e.message)
	}

	return result
}
