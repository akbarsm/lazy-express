module.exports = (instance) => (...column) => {
	instance.setHaving({havingBetween : column})

	return instance
}
