module.exports = (instance) => (...column) => {
	instance.setDistinct(column)

	return instance
}
