module.exports = (instance) => (...column) => {
	instance.setWhere({orWhereNotIn : column})

	return instance
}
