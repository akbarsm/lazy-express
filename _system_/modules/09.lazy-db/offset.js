module.exports = (instance) => (amount) => {
	instance.setOffset(amount)

	return instance
}
