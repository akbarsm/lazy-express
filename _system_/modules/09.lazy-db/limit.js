module.exports = (instance) => (amount) => {
	instance.setLimit(amount)

	return instance
}
