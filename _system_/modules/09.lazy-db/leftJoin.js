module.exports = (instance) => (...column) => {
	instance.setJoin({leftJoin : column})

	return instance
}
