module.exports = (instance) => (...column) => {
	instance.setJoin({outerJoin : column})

	return instance
}
