module.exports = (instance) => (...column) => {
	instance.setHaving({havingNotExists : column})

	return instance
}
