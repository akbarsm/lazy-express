module.exports = (instance) => (...column) => {
	instance.setHaving({havingExists : column})

	return instance
}
