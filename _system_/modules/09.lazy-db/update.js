module.exports = (instance) => async (paramData = {}, paramCondition = {}) => {
	let data = {}
	let result = {}
	let primary = instance.info().primary

	try {
		let alias = instance.info().alias		
		if(alias.length > 0){
			alias = `as ${alias}`
		}

		let knex = $knex(`${instance.info().table} ${alias}`.trim())

		if(paramData.$keys().length > 0){
			$foreach(paramData, (value, key) => {
				data[key] = value
			})
		} else if(instance.hasKey(primary)) {
			$foreach(instance, (value, key) => {
				if(!$isFunction(value) && key !== primary){
					data[key] = value
				}
			})
		}

		if(paramCondition.$keys().length > 0){
			$foreach(paramCondition, (value, key) => {
				knex.where(key, value)
			})
		} else if(instance.hasKey(primary)) {
			knex.where(primary, instance[primary])
		} else {
			[instance.getWhere(), instance.getHaving()].forEach((eachCondition) => {
				for (let condition of eachCondition) {
					$foreach(condition, (param, conditionType) => {
						knex[conditionType](...param)
					})
				}
			})
		}

		result = await knex.returning('*').update(data)

		result = result[0]

		instance.assign(result)
	} catch (error) {
		$log.error('LazyDB.update', error.message)
	}

	let temp = Object.create(instance)
	temp.assign(result)

	return temp
}
