module.exports = (instance) => (...column) => {
	instance.setWhere({whereNull : column})

	return instance
}
