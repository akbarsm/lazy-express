module.exports = (instance) => (...column) => {
	instance.setJoin({rightOuterJoin : column})

	return instance
}
