module.exports = (instance) => (...column) => {
	instance.setWhere({orWhereRaw : column})

	return instance
}
