module.exports = (instance) => (...column) => {
	instance.setJoin({fullOuterJoin : column})

	return instance
}
