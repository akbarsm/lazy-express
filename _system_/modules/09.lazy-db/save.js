module.exports = (instance) => async (param = {}) => {
	let data = {}
	let condition = {}
	let primary = instance.info().primary

	if(param.$keys().length > 0){
		$foreach(param, (value, key) => {
			data[key] = value
		})
	} else {
		$foreach(instance, (value, key) => {
			if(!$isFunction(value)){
				data[key] = value
			}
		})
	}

	let current = {}
	if(instance.hasKey(primary)) {
		condition[primary] = instance[primary]
		current = await instance.find(data[primary])
	}

	let result = {}
	if(instance.hasKey(primary) && current.$keys().length > 0){
		result = await instance.update(data, condition)
	} else {
		result = await instance.insert(data)
	}

	return result
}
