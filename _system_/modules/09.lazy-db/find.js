module.exports = (instance) => async (param) => {
	let result = undefined

	try {
		let alias = instance.info().alias
		if (alias.length > 0) {
			alias = `as ${alias}`
		}

		let knex = $knex(`${instance.info().table} ${alias}`.trim())

		if (instance.getSelect().length > 0) {
			knex.select(...instance.getSelect())
		}

		result = await knex.where(instance.info().primary, param).first()

		instance.reset()
	} catch (error) {
		$log.error('LazyDB.find', error.message)
	}

	let temp = Object.create(instance)
	if (result !== undefined) {
		temp.assign(result)
	}

	return result === undefined ? {} : temp
}
