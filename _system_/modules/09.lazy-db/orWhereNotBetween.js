module.exports = (instance) => (...column) => {
	instance.setWhere({orWhereNotBetween : column})

	return instance
}
