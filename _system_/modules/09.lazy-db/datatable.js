module.exports = (instance) => async (req, columnToSelect) => {
	req = { ...req.body(), ...req.param(), ...req.query() }

	let param = {
		draw: req.draw || 0,
		keyword: req.search ? req.search.value : '',
		start: parseInt(req.start || '0'),
		length: parseInt(req.length || '10'),
		searchColumns: [],
		orderColumns: [],
		orderIndex: parseInt(req.order ? req.order[0].column : '0'),
		orderType: req.order ? req.order[0].dir : 'asc',
	}
	let realColumns = {}
	let recordsCount = 0
	let results = []
	let disabledOrderCount = 0

	let copy = $db(instance.info().table, instance.info().primary, instance.info().alias)

	columnToSelect.forEach(sColumn => {
		let tempColumns = sColumn.toLowerCase().split(' as ')
		realColumns[tempColumns.length > 1 ? tempColumns[1] : tempColumns[0]] = tempColumns[0].trim()
	})

	realColumns.forEach(element => {
		/* Searchable */
		if (!param.keyword && element.searchable === 'true' && element.search.value) {
			param.keyword = element.search.value

			if ($isObject(element.data)) {
				param.searchColumns = element.data.$values()
			} else {
				param.searchColumns.push(element.data)
			}
		}

		/* Orderable */
		if (element.orderable === 'true') {
			param.orderColumns.push(isObject(element.data) ? element.data[element.data.$keys()[0]] : element.data)
		}

		disabledOrderCount += element.orderable === 'true' ? 0 : 1
	})

	if (!param.searchColumns.length) param.searchColumns = realColumns.$keys()

	instance.select(columnToSelect)
	copy.select($knex.raw(`count(*) as total`))

	if (param.keyword) {
		[copy, instance].forEach((_each) => {
			_each.where((qb) => {
				for (let eachColumn of param.searchColumns) {
					qb.orWhere($knex.raw(`CAST(${realColumns[eachColumn]} AS VARCHAR)`), 'ilike', `%${param.keyword}%`)
				}
			})
		})
	}

	[instance.getWhere(), instance.getHaving()].forEach((eachCondition) => {
		for (let condition of eachCondition) {
			$foreach(condition, (param, conditionType) => {
				copy[conditionType](...param)
			})
		}
	})

	instance.getJoin().forEach((join) => {
		join.forEach((values, key) => {
			copy[key](...values)
		})
	})

	recordsCount = +(await copy.first()).total

	let column
	let direction

	if (param.orderColumns.length > 0) {
		column = param.orderColumns[param.orderIndex - disabledOrderCount]
		direction = param.orderType
	} else if (instance.getOrder().$keys().length > 0) {
		column = instance.getOrder().$keys()[0]
		direction = instance.getOrder().$values()[0]
	} else {
		column = columnToSelect[0]
		direction = 'asc'
	}

	let tempResult = await instance.offset(param.start).limit(param.length).orderBy(column, direction).get()
	for (let index = 0; index < tempResult.length; index++) {
		let row = {}

		row.index = param.start + (index + 1)
		row.dt_row_index = param.start + (index + 1)

		row = { ...row, ...tempResult[index] }

		results.push(row)
	}

	return {
		draw: param.draw,
		recordsTotal: recordsCount,
		recordsFiltered: recordsCount,
		data: results,
	}
}
