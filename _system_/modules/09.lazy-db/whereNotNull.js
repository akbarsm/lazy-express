module.exports = (instance) => (...column) => {
	instance.setWhere({whereNotNull : column})

	return instance
}
