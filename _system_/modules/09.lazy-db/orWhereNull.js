module.exports = (instance) => (...column) => {
	instance.setWhere({orWhereNull : column})

	return instance
}
