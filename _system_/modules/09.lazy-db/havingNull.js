module.exports = (instance) => (...column) => {
	instance.setHaving({havingNull : column})

	return instance
}
