module.exports = (instance) => async () => {
	let result = null

	try {
		let alias = instance.info().alias		
		if(alias.length > 0){
			alias = `as ${alias}`
		}

		let knex = $knex(`${instance.info().table} ${alias}`.trim())

		result  = await knex.delete()
		result = result > 0

		instance.reset()
	} catch (error) {
		$log.error('LazyDB.truncate', error.message)
	}

	return result
}
