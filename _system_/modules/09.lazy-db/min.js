module.exports = (instance) => async (column) => {
	let result = undefined

	try {
		result = await $knex(instance.info().table).min(column)
		result = +result[0].min
		instance.reset()
	} catch (error) {
		$log.error('LazyDB.min', e.message)
	}

	return result
}
