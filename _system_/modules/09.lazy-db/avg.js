module.exports = (instance) => async (column) => {
	let result = undefined

	try {
		result = await $knex(instance.info().table).avg(column)
		result = +result[0].avg
		instance.reset()
	} catch (error) {
		$log.error('LazyDB.avg', e.message)
	}

	return result
}
