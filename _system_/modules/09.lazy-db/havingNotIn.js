module.exports = (instance) => (...column) => {
	instance.setHaving({havingNotIn : column})

	return instance
}
