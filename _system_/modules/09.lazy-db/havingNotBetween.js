module.exports = (instance) => (...column) => {
	instance.setHaving({havingNotBetween : column})

	return instance
}
