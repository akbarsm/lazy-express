module.exports = (instance) => async (param = {}) => {
	let data = {}
	let result = undefined

	if(param.$keys().length > 0){
		$foreach(param, (value, key) => {
			data[key] = value
		})
	} else {
		$foreach(instance, (value, key) => {
			if(!$isFunction(value)){
				data[key] = value
			}
		})
	}

	try {
		let alias = instance.info().alias		
		if(alias.length > 0){
			alias = `as ${alias}`
		}

		let knex = $knex(`${instance.info().table} ${alias}`.trim())

		result = await knex.returning('*').insert(data)
		result = result[0]
	} catch (error) {
		$log.error('LazyDB.insert', error.message)
	}

	let temp = Object.create(instance)
	temp.assign(result)

	return temp
}
