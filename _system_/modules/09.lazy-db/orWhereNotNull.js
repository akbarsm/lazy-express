module.exports = (instance) => (...column) => {
	instance.setWhere({orWhereNotNull : column})

	return instance
}
