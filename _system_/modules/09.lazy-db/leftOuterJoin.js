module.exports = (instance) => (...column) => {
	instance.setJoin({leftOuterJoin : column})

	return instance
}
