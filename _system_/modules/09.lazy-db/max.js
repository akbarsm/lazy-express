module.exports = (instance) => async (column) => {
	let result = undefined

	try {
		result = await $knex(instance.info().table).max(column)
		result = +result[0].max
		instance.reset()
	} catch (error) {
		$log.error('LazyDB.max', e.message)
	}

	return result
}
