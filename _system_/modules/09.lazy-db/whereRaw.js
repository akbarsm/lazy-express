module.exports = (instance) => (...column) => {
	instance.setWhere({whereRaw : column})

	return instance
}
