module.exports = (instance) => (...column) => {
	instance.setHaving({havingNotNull : column})

	return instance
}
