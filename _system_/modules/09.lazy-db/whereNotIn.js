module.exports = (instance) => (...column) => {
	instance.setWhere({whereNotIn : column})

	return instance
}
