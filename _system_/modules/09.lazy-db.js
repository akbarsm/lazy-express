const chalk = require('chalk')
const knex = require('knex')

if (!global.hasKey('$knex')) {
	if ($config('database').enabled) {
		global.$knex = knex({
			client: $config('database').type,
			connection: {
				host: $config('database').host,
				user: $config('database').username,
				password: $config('database').password,
				database: $config('database').name,
			},
			searchPath: [$config('database').schema]
		})

		if($config('database').logging){
			$knex.on('query', (rawQuery) => {
				let bindings = rawQuery.bindings
				let query = rawQuery.sql

				bindings.forEach((each, index) => {
					query = query.replace('$' + (index+1), chalk.greenBright(each))
				})

				console.log(chalk.yellowBright('[Raw Query]') + '\n' + query + '\n')
			})
		}

		let info = {
			connected: false,
			message: ''
		}
		$knex.raw(`SELECT 'OK' AS "status"`).then((response) => {
			if (response.rows[0].status) {
				info.connected = true
			}
		}).catch((error) => {
			info.message = error.message
		}).finally(() => {
			global.$knexInfo = info

			$databaseCallback()
			delete global.$databaseCallback
		})
	}
}

module.exports = [
	{
		name: 'LazyDB',
		action: class LazyDB {
			#distinct = []
			#group = []
			#having = []
			#join = []
			#limit = -1
			#offset = -1
			#order = {}
			#select = []
			#where = []

			constructor() {
				$file.list($root('_system_/modules/09.lazy-db'), (name, path, isFile) => {
					if (isFile && name.endsWith('.js')) {
						let method = name.$trim(".js")
						this[method] = require(path)(this)
					}
				})

				this['props'] = (key = '') => {
					let props = {}
					$foreach(this, (value, propsKey) => {
						if(!$isFunction(value)){
							props[propsKey] = value
						}
					})
	
					return key ? props[key] : props
				}
			}

			info() {
				return {
					table: '',
					primary: '',
					alias: ''
				}
			}

			reset() {
				this.#distinct = []
				this.#group = []
				this.#having = []
				this.#join = []
				this.#limit = -1
				this.#offset = -1
				this.#order = {}
				this.#select = []
				this.#where = []
			}

			assign(object){
				$foreach(object, (value, key) => {
					this[key] = value
				})
			}

			instance(){
				return Object.create(this)
			}

			/* 
			 * Getters
			 */
			getDistinct() {
				return this.#distinct
			}

			getGroup() {
				return this.#group
			}

			getHaving(){
				return this.#having
			}

			getJoin(){
				return this.#join
			}

			getLimit() {
				return this.#limit
			}

			getOffset() {
				return this.#offset
			}

			getOrder(){
				return this.#order
			}

			getSelect() {
				return this.#select
			}

			getWhere() {
				return this.#where
			}

			/* 
			 * Setters
			 */
			setDistinct(column) {
				this.#distinct.push(...column)
			}

			setGroup(column) {
				this.#group.push(...column)
			}

			setHaving(param){
				this.#having.push(param)
			}

			setJoin(param){
				this.#join.push(param)
			}

			setLimit(amount) {
				this.#limit = amount
			}

			setOffset(amount) {
				this.#offset = amount
			}

			setOrder(column, direction){
				this.#order[column] = direction
			}

			setSelect(column) {
				this.#select.push(...column)
			}

			setWhere(column) {
				this.#where.push(column)
			}
		}
	},
	{
		name: '$query',
		action: async (query, param) => {
			if ($config('database').enabled && global.$knexInfo.connected) {
				try {
					let result = await $knex.raw(query, param)

					return result.rows
				} catch (error) {
					$log.error('LazyDB.query', error.message)
				}

				return null
			} else {
				$log.error('LazyDB.query', 'Database connection is disabled or failed to connect')
			}
		}
	},
	{
		name : '$db',
		action: (table, primary = 'id', alias = '') => {
			if(!$config('database').enabled){
				$log.warning('LazyDB.$db', 'Database connection is disabled, cannot make $db instance')
			} else if(!$knexInfo.connected){
				$log.error('LazyDB.$db', `Database connection failed, cannot make $db instance. (${$knexInfo.message})`)
			} else {
				class DB extends LazyDB {
					info() {
						return {
							table : table,
							primary : primary,
							alias : alias
						}
					}
				}

				return new DB
			}

			return null
		}
	}
]
