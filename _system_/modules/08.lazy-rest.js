const axios = require('axios')

const ajax = async (...param) => {
	let method = param[0]
	param.shift()

	let response = await axios[method](...param)

	return {
		status : response.status,
		statusText : response.statusText,
		data : response.data
	}
}

module.exports = [
	{
		name: '$delete',
		action:  async (...param) => {
			param.unshift('delete')
			return await ajax(...param)
		}
	},
	{
		name: '$get',
		action:  async (...param) => {
			param.unshift('get')
			return await ajax(...param)
		}
	},
	{
		name: '$head',
		action:  async (...param) => {
			param.unshift('head')
			return await ajax(...param)
		}
	},
	{
		name: '$patch',
		action:  async (...param) => {
			param.unshift('patch')
			return await ajax(...param)
		}
	},
	{
		name: '$post',
		action:  async (...param) => {
			param.unshift('post')
			return await ajax(...param)
		}
	},
	{
		name: '$put',
		action:  async (...param) => {
			param.unshift('put')
			return await ajax(...param)
		}
	},
	
]
