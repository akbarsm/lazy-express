
const bcryptjs = require('bcryptjs')
const md5 = require('md5')
const sha1 = require('sha1')
const underscore = require('underscore')
const uuid = require('uuid').v1

const attach = (methodName, action) => {
	Object.defineProperty(String.prototype, methodName, {
		value: action,
		writable: true,
		configurable: true,
		enumerable: false
	})
}

module.exports = [
	{
		name : '$random',
		action : function(length = 4, pattern = '0123456789ABCDEFabcdef'){
			pattern = pattern.split("")

			return underscore.sample(pattern, length).join("")
		}
	},
	
	{
		name: '$uuid',
		action: function(){
			return uuid()
		}
	},

	{
		autoload: () => {
			attach('contains', function (pattern) {
				return this.search(pattern) >= 0
			})

			attach('cutLeft', function (amount) {
				return this.substr(amount, this.length)
			})

			attach('cutRight', function (amount) {
				return this.substr(0, this.length - amount)
			})

			attach('decrypt', function (string) {
				return bcryptjs.compareSync(string, String(this))
			})

			attach('encrypt', function (saltRound = 8) {
				return bcryptjs.hashSync(String(this), saltRound)
			})

			attach('md5', function () {
				return md5(this)
			})

			attach('sha1', function () {
				return sha1(this)
			})

			attach('splitWords', function(amount = 1){
				let word = ""
				let words = this.split(/\s+/gi)

				for(let i = 0; i<amount; i++){
					word += words[i] + " "
				}

				return word.trim()
			})

			attach('subLeft', function(amount){
				return this.substring(0, amount)
			})

			attach('subRight', function(amount){
				return this.substring(this.length - amount, this.length)
			})

			attach('substr', function(from, to){
				return this.substring(from, to)
			})

			attach('toTitleCase', function(){
				let capitalized = ""

				for(let phrase of String(this).toLocaleLowerCase().trim().split(/\s+/gi)){
					capitalized += String(phrase).toUpperCase().substring(0, 1) + String(phrase).toLowerCase().substring(1) + " "
				}

				return capitalized.trim()
			})

			attach('$trim', function(char = " "){
				let value = this

				while(value.startsWith(char)){
					value = value.cutLeft(char.length)
				}

				while(value.endsWith(char)){
					value = value.cutRight(char.length)
				}

				return value
			})

			attach('trimLeft', function(char = " "){
				let value = this

				while(value.startsWith(char)){
					value = value.cutLeft(char.length)
				}

				return value
			})

			attach('trimRight', function(char = " "){
				let value = this

				while(value.endsWith(char)){
					value = value.cutRight(char.length)
				}

				return value
			})
		}
	}
]
