const fs = require('fs')
const fse = require('fs-extra')
const chalk = require('chalk')

const log = require('./01.lazy-logger')[0].action
const parse = (fullPath) => {
	fullPath = String(fullPath).replace(/\\/gi, "/")

	let fileName = fullPath.split("/").pop()
	let basePath = fullPath.replace(`/${fileName}`, "")

	return {
		basePath, fullPath
	}
}


module.exports = [
	{
		name: '$file',
		action: {
			append: (path, data) => {
				try {
					let { basePath, fullPath } = parse(path)

					fs.mkdirSync(basePath, { recursive: true })
					fs.appendFileSync(fullPath, data)
				} catch (error) {
					$log.error('LazyFile.append', `${path} ${error.message}`)
					return false
				}

				return true
			},

			copy: (source, destination, overwrite = true) => {
				try {
					source      = parse(source).fullPath;
					destination = parse(destination).fullPath;
	
					fse.copySync(source, destination, {overwrite: overwrite}, function (err) {
						if (err) {
							$log.error('LazyFile.copy', `${source}, ${destination} ${error.message}`);
						}
					});
				} catch (error) {
					$log.error('LazyFile.copy', `${source}, ${destination} ${error.message}`);
					return false;
				}
	
				return true;
			},

			delete: (...paths) => {
				paths.forEach((each, index) => {
					try {
						let path = parse(each).fullPath
						if (fs.existsSync(path)) {
							if (fs.lstatSync(path).isDirectory()) {
								fs.rmdirSync(path, { recursive: true })
							} else {
								fs.unlinkSync(path)
							}
						}
					} catch (error) {
						$log.error('LazyFile.delete', `${path} ${error.message}`)
						return false
					}
				})

				return true
			},

			exists: (rawPath) => {
				let path = parse(rawPath).fullPath

				return fs.existsSync(path)
			},

			isDir: (path) => {
				if (fs.existsSync(path)) {
					return fs.lstatSync(path).isDirectory()
				} else {
					$log.error('LazyFile.isDir', `path ${chalk.bold(path)} does not exists`)
				}
				return false
			},

			isFile: (path) => {
				if (fs.existsSync(path)) {
					return fs.lstatSync(path).isFile()
				} else {
					$log.error('LazyFile.isFile', `path ${chalk.bold(path)} does not exists`)
				}
				return false
			},

			list: (directory, callback, recursive = false) => {
				const path = directory

				if (fs.existsSync(path)) {
					fs.readdirSync(path).forEach((eachFile) => {
						const file = eachFile
						const staticPath = path + "/" + file

						if (recursive && fs.lstatSync(staticPath).isDirectory()) {
							list(staticPath, callback, true)
						}

						callback(file, staticPath, fs.lstatSync(staticPath).isFile())
					})
				} else {
					$log.error('LazyFile.list', `path ${chalk.bold(path)} does not exists`)
				}
			},

			mkdirs: (path) => {
				path = parse(path).fullPath

				return fs.mkdirSync(path, { recursive: true })
			},

			move: (source, destination) => {
				if (fs.existsSync(source)) {
					try {
						source = parse(source).fullPath
						destination = parse(destination).fullPath

						fse.moveSync(source, destination, { overwrite: true }, function (err) {
							if (err) {
								$log.error('LazyFile.move', `${chalk.bold(source)}, ${chalk.bold(destination)} ${error.message}`)
							}
						})

						return fs.existsSync(destination)
					} catch (error) {
						$log.error('LazyFile.move', `${source}, ${destination} ${error.message}`)
					}
				} else {
					$log.error('LazyFile.move', `${chalk.bold(source)} does not exists`)
				}

				return false
			},

			read: (source, encoding = "utf-8") => {
				if (fs.existsSync(source)) {
					if (fs.lstatSync(source).isFile()) {
						try {
							source = parse(source).fullPath;
	
							return fs.readFileSync(source, encoding);
						} catch (error) {
							$log.error('LazyFile.read', `${source} ${error.message}`);
						}
					} else {
						$log.error('LazyFile.read', `${source} is not a file`);
					}
				} else {
					$log.error('LazyFile.read', `${source} does not exists`);
				}
	
				return false;
			},

			write: (source, data, encoding = "utf-8") => {
				try {
					source = parse(source).fullPath

					let basePath = parse(source).basePath

					if (source.includes('/')) fs.mkdirSync(basePath, { recursive: true })

					fs.writeFileSync(source, data, encoding)

					return fs.existsSync(source)
				} catch (error) {
					$log.error('LazyFile.write', `${chalk.bold(source)} ${error.message}`)
					return false
				}
			}
		}
	}
]
