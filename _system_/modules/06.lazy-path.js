const path = require('path')

module.exports = [
	{
		name: '$config',
		action: (section = '') => {
			return require($root("config/" + section))
		}
	},
	{
		name: '$controller',
		action: (controller = '') => {
			return require($root('controller/' + controller))
		}
	},
	{
		name: '$middleware',
		action: (middleware = '') => {
			return require($root('middleware/' + middleware))
		}
	},
	{
		name: '$model',
		action: (model = '') => {
			return require($root('model/' + model))
		}
	},
	{
		name: '$root',
		action: (subdir = '') => {
			if (subdir) {
				subdir = "/" + subdir
			}
			return path.resolve(process.cwd()) + subdir
		}
	}
]
