module.exports = {
	prefix: "not_regex",
	action: (req, data, options, key, rules, values, keyIsArray, valueIsArray) => {
		options = (options || "")
		if (new RegExp(options).test(data[key])) {
			return `'${key}' must not have regex pattern of '${options}'`
		}

		return null
	}
}
