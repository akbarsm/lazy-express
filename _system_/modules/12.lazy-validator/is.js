module.exports = {
	prefix: "is",
	action: (req, data, options, key, rules, values, keyIsArray, valueIsArray) => {
		if (!["alpha", "alphadash", "alphanumeric", "array", "email", "file", "numeric", "string"].includes(options)) {
			$log.warning(`LazyValidator`, `'is:${options}' rule does not exists!`)
		} else if (rules.includes("required")) {
			let isFile = req.files().hasKey(key)

			if (options === "alpha" && !$isAlpha(data[key])) {
				return `'${key}' must be in 'alpha (a-z,A-Z)' format`

			} else if (options === "alphadash" && !$isAlphaDash(data[key])) {
				return `'${key}' must be in 'alphadash (a-z, A-Z, -, _)' format`

			} else if (options === "alphanumeric" && !$isAlphaNumeric(data[key])) {
				return `'${key}' must be in 'alphanumeric (a-z, A-Z, 0-9)' format`

			} else if (options === "array" && !(data[key] instanceof Array)) {
				return `'${key}' must be an array instance`

			} else if (options === "email" && !$isEmail(data[key])) {
				return `'${key}' must be in 'email' format`

			} else if (options === "file" && !isFile) {
				return `'${key}' must be a 'file'`

			} else if (options === "numeric" && !$isNumeric(data[key])) {
				return `'${key}' must be in 'numeric (0-9)' format`

			} else if (options === "string" && !$isString(data[key])) {
				return `'${key}' must be in 'string (a-z, A-Z)' format`
			}
		}

		return null
	}
}
