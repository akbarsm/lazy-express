module.exports = {
	prefix: "required",
	action: (req, data, options, key, rules, values, keyIsArray, valueIsArray) => {
		let errorIndex = []

		values.forEach((each, index) => {
			if (!each || typeof each === "object" && each.length === 0) {
				errorIndex.push(index)
			}
		})

		if (errorIndex.length) {
			return `'${key}' ${keyIsArray ? `(index ${errorIndex.join(",")})` : ""}is required`
		}

		return null
	}
}
