module.exports = {
	prefix: "ext",
	action: (req, data, options, key, rules, values, keyIsArray, valueIsArray) => {
		options = (options || "")

		let isFile = req.files().hasKey(key)

		if (!(isFile && options.split(",").includes(data[key].ext))) {
			return `'${key}' must be a file with extension '${options.split(",").join(",")}'`
		}

		return null
	}
}
