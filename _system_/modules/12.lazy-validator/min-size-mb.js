module.exports = {
	prefix: "min-size-mb",
	action: (req, data, options, key, rules, values, keyIsArray, valueIsArray) => {
		options = (options || "")

		let isFile = req.files().hasKey(key)

		if (!(isFile && +options < data[key].sizeInMB)) {
			return `'${key}' must be a file with minimum size of '${options}MB'`
		}

		return null
	}
}
