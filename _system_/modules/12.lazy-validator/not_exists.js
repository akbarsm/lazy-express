module.exports = {
	prefix: "not_exists",
	action: async (req, data, options, key, rules, values, keyIsArray, valueIsArray) => {
		if ($knexInfo.connected) {
			options = (options || "")

			let info = options.split(",")

			if ([1,2].includes(info.length)) {
				let table = info[0].trim()
				let	column = info.length === 2 ? info[1].trim() : key
				let current = undefined

				current = await $db(table).where(column, data[key]).exists()

				if (current) {
					return `'${key}' of '${data[key]}' already exists in other record`
				}
			} else {
				$log.warning(`LazyValidation.not_exists`, `This rule required 2 parameters -> not_exists:table,column`)
			}

			return null
		} else {
			$log.warning("LazyValidation.not_exists", "Skipping this validation as database connection is not established")
		}
	}
}
