module.exports = {
	prefix: "min",
	action: (req, data, options, key, rules, values, keyIsArray, valueIsArray) => {
		options = (options || "")
		if (data[key].length < parseInt(options)) {
			return `'${key}' minimum length must be '${options}'`
		}

		return null
	}
}
