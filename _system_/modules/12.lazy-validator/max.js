module.exports = {
	prefix: "max",
	action: (req, data, options, key, rules, values, keyIsArray, valueIsArray) => {
		options = (options || "")
		if (data[key].length > parseInt(options)) {
			return `'${key}' maximum length must be '${options}'`
		}

		return null
	}
}
