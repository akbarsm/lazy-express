module.exports = {
	prefix: "not_in",
	action: (req, data, options, key, rules, values, keyIsArray, valueIsArray) => {
		options = (options || "")
		if (options.split(",").includes(data[key])) {
			return `'${key}' value must not be between '${options}'`
		}

		return null
	}
}
