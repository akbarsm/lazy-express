module.exports = {
	prefix: "exists",
	action: async (req, data, options, key, rules, values, keyIsArray, valueIsArray) => {
		let current = undefined
		if ($knexInfo.connected) {
			if (data[key]) {
				options = (options || "")

				let info = options.split(",")


				if ([1, 2].includes(info.length)) {
					let table  = info[0].trim()
					let	column = info.length === 2 ? info[1].trim() : key

					current = await $db(table).where(column, data[key]).first()

					if (current.isEmpty()) {
						return `'${key}' of '${data[key]}' does not exists`
					}
				} else {
					$log.warning(`LazyValidation.exists`, `This rule required maximum 2 parameters -> exists:table,[column], if column is not defined then it will take same field as the key being validated`)
				}
			}

			return current
		} else {
			$log.warning("LazyValidation.exists", "Skipping this validation as database connection is not established")
		}
	}
}
