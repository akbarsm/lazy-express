module.exports = {
	prefix: "mime",
	action: (req, data, options, key, rules, values, keyIsArray, valueIsArray) => {
		options = (options || "")

		let isFile = req.files().hasKey(key)

		if (!(isFile && options.split(",").includes(data[key].mime))) {
			return `'${key}' must be a file with mime type of '${options.split(",").join(",")}'`
		}

		return null
	}
}
