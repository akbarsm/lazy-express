module.exports = {
	prefix: "min-size-kb",
	action: (req, data, options, key, rules, values, keyIsArray, valueIsArray) => {
		options = (options || "")

		let isFile = req.files().hasKey(key)

		if (!(isFile && +options < data[key].sizeInKB)) {
			return `'${key}' must be a file with minimum size of '${options}KB'`
		}

		return null
	}
}
