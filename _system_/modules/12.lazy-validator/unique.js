module.exports = {
	prefix: "unique",
	action: async (req, data, options, key, rules, values, keyIsArray, valueIsArray) => {
		if ($knexInfo.connected) {
			if (data[key]) {
				options = (options || "")

				let info = options.split(",")

				let table = info[0].trim()
				let column = info.length < 2 ? key : info[1].trim()
				let owner = info.length === 3 ? info[2].split("=") : ["id", undefined]
				let ownerColumn = owner[0].trim()
				let ownerValue = owner[1]
				let current = undefined

				if (owner[1] !== undefined && owner[1].contains('\\$')) {
					ownerValue = data[owner[1].replace('$', '')]
				}

				current = await $db(table).where(column, data[key]).first()

				if (typeof ownerValue === "number") {
					ownerValue = +ownerValue
				}

				if (!current.isEmpty()) {
					if (info.length < 3 && current) {
						return `'${key}' of '${data[key]}' already exists`
					} else if (info.length === 3 && String(current[ownerColumn]) !== String(ownerValue)) {
						return `'${key}' of '${data[key]}' already used by other record`
					}
				}
			}

			return null
		} else {
			$log.warning("LazyValidation.unique", "Skipping this validation as database connection is not established")
		}
	}
}
