module.exports = {
	prefix: "confirmed",
	action: (req, data, options, key, rules, values, keyIsArray, valueIsArray) => {
		options = (options || "")
		if (data[key] !== req[options]) {
			return `'${key}' must equals to '${options}'`
		}

		return null
	}
}
