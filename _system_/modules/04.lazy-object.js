const attach = (methodName, action) => {
	Object.defineProperty(Object.prototype, methodName, {
		value: action,
		writable: true,
		configurable: true,
		enumerable: false
	})
}

module.exports = [
	{
		autoload: () => {
			attach('contains', function (value) {
				return Object.values(this).includes(value)
			})

			attach('isEmpty', function (value) {
				return Object.keys(this).length === 0
			})

			attach('except', function (...keys) {
				if (keys.length === 1 && typeof keys[0] === "object") {
					keys = keys[0]
				}
		
				let filteredKeys = Object.keys(this).filter((key) => {
					return !keys.includes(key)
				})
		
				let filteredObject = {};
				filteredKeys.forEach((key) => {
					filteredObject[key] = this[key]
				})
		
				return filteredObject
			})

			attach('forEach', function (callback) {
				Object.keys(this).forEach((key) => {
					callback(this[key], key)
				})
			})

			attach('hasKey', function (key) {
				return Object.keys(this).includes(key)
			})

			attach('hasValue', function (value) {
				return Object.values(this).includes(value)
			})

			attach('$keys', function () {
				return Object.keys(this)
			})

			attach('only', function (...keys) {
				if (keys.length === 1 && typeof keys[0] === "object") {
					keys = keys[0]
				}
		
				let filteredKeys = Object.keys(this).filter((key) => {
					return keys.includes(key)
				})
		
				let filteredObject = {}
				filteredKeys.forEach((key) => {
					filteredObject[key] = this[key]
				})
		
				return filteredObject
			})

			attach('$values', function(){
				return Object.values(this)
			})
		}
	}
]
