const types = require('pg').types
types.setTypeParser(types.builtins.TIMESTAMPTZ, (val) => val)
types.setTypeParser(types.builtins.TIMESTAMP, (val) => val)

process.stdout.write("\x1Bc")

require('dotenv').config()
require('module-alias/register')

const chalk = require('chalk')
const info = require(process.cwd() + '/package.json')
const format = require('fast-printf').printf

let totalElapsedTime = 0.0

const cmList = (type, callback) => {
	let longestName = 0
	$file.list($root('model'), (file, path, isFile) => {
		if (isFile && file.endsWith('.js')) {
			if (file.length - 3 > longestName) longestName = file.length - 3
			callback(file)
		}
	})
}

const header = () => {
	console.log(`.------------------------.`)
	console.log(`| ${chalk.bold(chalk.yellow('Lazy Express Framework'))} |`)
	console.log(`'------------------------'`)
	console.log(``)
	console.log(` Author\t\t: ${chalk.greenBright(info.author)}`)
	console.log(` Version\t: ${chalk.greenBright(info.version)}`)
	console.log(` Description\t: ${chalk.greenBright(info.description)}`)
	console.log(` Repository\t: ${chalk.greenBright(info.repository.url)}`)
	console.log(` License\t: ${chalk.greenBright(info.license)}`)
	console.log(` Developer Note\t: ${chalk.redBright('This module / library is still in development stage and provided as is without warranty')}`)
	console.log(``)
}

const modules = () => {
	console.log(`.----------------------.`)
	console.log(`| ${chalk.bold(chalk.yellow('Modules Installation'))} |`)
	console.log(`'----------------------'`)
	console.log(``)

	const loadDirs = (directory, callback, recursive = false) => {
		let path = (directory)
		let fs = require('fs')

		fs.readdirSync(path).forEach((eachFile) => {
			let file = eachFile
			let staticPath = path + "/" + file

			if (recursive && fs.lstatSync(staticPath).isDirectory()) {
				loadDirs(staticPath, callback, true)
			}

			callback(file, staticPath, fs.lstatSync(staticPath).isFile())
		})
	}

	let longestName = 0
	let paths = []

	loadDirs(process.cwd() + '/_system_/modules', (file, path, isFile) => {
		if (isFile && file.endsWith('.js')) {
			let length = file.length - 6
			longestName = length > longestName ? length : longestName
			paths.push({
				name: file.split('.')[1],
				path: path
			})
		}
	}, false)

	paths.forEach(each => {
		let start = Date.now()
		let moduleList = require(each.path)

		moduleList.forEach(module => {
			let keys = Object.keys(module)

			if (keys.includes('autoload')) {
				module.autoload()
			}

			if (keys.includes('action') && keys.includes('name')) {
				global[module.name] = module.action
			}
		})

		let end = Date.now()
		let elapsed = (end - start) / 1000

		totalElapsedTime += elapsed

		let nameTemplate = chalk.greenBright(`%-${longestName}s`)
		let timeTemplate = chalk.greenBright('%f')

		console.log(format(`Loading modules ${nameTemplate} .... ${timeTemplate} seconds`, each.name, elapsed.toFixed(3)))
	})

	console.log(``)
	console.log(format(`Total elapsed loading time ${chalk.greenBright('%f')} seconds`, totalElapsedTime.toFixed(3)))
	console.log(``)
}

const databaseCallback = () => {
	let status = chalk.yellowBright('DISABLED')
	let message = chalk.yellowBright(`Database connection is disabled, set 'DATABASE_ENABLED' to 'true' in env to enable it`)

	if ($config('database').enabled) {
		if ($knexInfo.connected) {
			status = chalk.greenBright('CONNECTED')
			message = chalk.greenBright('Database connection successfully established')
		} else {
			status = chalk.redBright('NOT CONNECTED')
			message = chalk.redBright($knexInfo.message)
		}
	}

	console.log(`.-----------------.`)
	console.log(`| ${chalk.bold(chalk.yellow('Database Status'))} |`)
	console.log(`'-----------------'`)
	console.log(``)
	console.log(` Status\t\t: ${status}`)
	console.log(` Info\t\t: ${message}`)
	console.log(``)

	model()
}

const model = () => {
	console.log(`.---------------------------.`)
	console.log(`| ${chalk.bold(chalk.yellow('Checking Available Models'))} |`)
	console.log(`'---------------------------'`)
	console.log(``)

	let longestName = 0
	let names = []
	cmList('model', (file) => {
		names.push(file.replace('.js', ''))
	})

	if (names.length > 0) {
		names.forEach(name => {
			let modelFormat = chalk.greenBright(`%-${longestName}s`)
			$log.format(`Model ${modelFormat} available`, name)
		})
	} else {
		console.log(chalk.yellowBright("You haven't created any model"))
	}

	console.log(``)

	controller()
}

const controller = () => {
	console.log(`.--------------------------------.`)
	console.log(`| ${chalk.bold(chalk.yellow('Checking Available Controllers'))} |`)
	console.log(`'--------------------------------'`)
	console.log(``)

	let longestName = 0
	let names = []
	cmList('controller', (file) => {
		names.push(file.replace('.js', ''))
	})

	if (names.length > 0) {
		names.forEach(name => {
			let controllerFormat = chalk.greenBright(`%-${longestName}s`)
			$log.format(`Controller ${controllerFormat} available`, name)
		})
	} else {
		console.log(chalk.yellowBright("You haven't created any controller"))
	}

	console.log(``)
	console.log(chalk.magenta(chalk.bold("Make sure to register your controller in config/routes")))
	console.log(``)

	routes()

}

const routes = async () => {
	console.log(`.------------------------------.`)
	console.log(`| ${chalk.bold(chalk.yellow('Registering Available Routes'))} |`)
	console.log(`'------------------------------'`)
	console.log(``)

	await $jwt.initJwt()

	$server.registerRoutes()
	$server.start()
}

global.$databaseCallback = databaseCallback

header()
modules()

if (!$config('database').enabled) {
	databaseCallback()
}
